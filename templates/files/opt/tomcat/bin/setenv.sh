export CATALINA_OPTS="$CATALINA_OPTS -Xms128m -Djava.security.egd=file:/dev/./urandom -Djava.awt.headless=true"
export CATALINA_OPTS="$CATALINA_OPTS -Xmx{{tomcat_jvm_heap_size}}"
