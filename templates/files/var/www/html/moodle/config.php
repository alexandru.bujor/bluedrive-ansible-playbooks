<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mariadb';
$CFG->dblibrary = 'native';
$CFG->dbhost    = '{{ moodle_db_host }}';
$CFG->dbname    = '{{ moodle_db }}';
$CFG->dbuser    = '{{ moodle_db_user }}';
$CFG->dbpass    = '{{ moodle_db_password }}';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => '',
  'dbsocket' => '',
);


{% if httpd_redirect_to_https %}
$CFG->wwwroot   = 'https://{{full_hostname}}/moodle';
{% else %}
$CFG->wwwroot   = 'http://{{full_hostname}}/moodle';
{% endif %}

$CFG->dataroot  = '{{ moodle_data_folder }}';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 0777;

require_once(dirname(__FILE__) . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
