<?php

$sugar_config_si = array(
'setup_db_host_name' => '{{ sugarcrm_db_host }}',
'setup_db_sugarsales_user' => '{{ sugarcrm_db_user }}',
'setup_db_sugarsales_password' => '{{ sugarcrm_db_password }}',
'setup_db_database_name' => '{{ sugarcrm_db }}',
'setup_db_type' => 'mysql',
'setup_db_pop_demo_data' => false,

'setup_db_create_database' => 1,
'setup_db_create_sugarsales_user' => 0,
'dbUSRData' => 'create',
'setup_db_drop_tables' => 1,
'setup_db_username_is_privileged' => true,

'setup_db_admin_user_name' => '{{ mysql_root_user }}',
'setup_db_admin_password' => '{{ mysql_root_password }}',

'setup_fts_type' => 'Elastic',
'setup_fts_host' => 'localhost',
'setup_fts_port' => '9200',

'setup_site_url' => 'http://{{ full_hostname }}',
'setup_site_admin_user_name'=>'{{ sugarcrm_admin_username }}',
'setup_site_admin_password' => '{{ sugarcrm_admin_password }}',
'setup_license_key' => 'LICENSE_KEY',

'setup_site_sugarbeet_automatic_checks' => true,

'default_currency_iso4217' => 'USD',
'default_currency_name' => 'US Dollar',
'default_currency_significant_digits' => '2',
'default_currency_symbol' => '$',
'default_date_format' => 'Y-m-d',
'default_time_format' => 'H:i',
'default_decimal_seperator' => '.',
'default_export_charset' => 'ISO-8859-1',
'default_language' => 'en_us',
'default_locale_name_format' => 's f l',
'default_number_grouping_seperator' => ',',
'export_delimiter' => ',',

'setup_system_name' => 'SugarCRM - Commercial Open Source CRM',
);

?>
