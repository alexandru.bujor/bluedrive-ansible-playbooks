# Bluedrive Ansible Playbooks

## Intro
Designed and implemented by **Bluedrive Solutions (http://www.bluedrive.ro)**

Main Contributor: **Alexandru Bujor (alexandru.bujor@bluedrive.ro)**

## Contents
This repository contains a collection of Ansible Playbooks which can be used to quickly deploy some nice tools into your infrastructure with minimal effort. Most of the stuff included was tested on CentOS/RHEL 7, with a big chance to work on CentOS/RHEL 6.

Included Service Playbooks
* Nextcloud (nextcloud.yml) - Tested on EL 7
* CachetHQ (cachethq.yml) - Tested on EL 6 and EL 7
* Elasticsearch (elasticsearch.yml) - Tested on EL 6 and EL 7
* Logstash (logstash.yml) - Tested on EL 6 and EL 7
* Kibana (kibana.yml)- Tested on EL 6 and EL 7
* Jenkins (jenkins.yml) - Tested on EL 6 and EL 7
* Tomcat (tomcat.yml) - Tested on EL 6 and EL 7
* HTTPD (httpd.yml) - Tested on EL 6 and EL 7
* PHP 5.6 (php56.yml) - Tested on EL 6 and EL 7
* PHP (php.yml) - Tested on EL 6 and EL 7
* Zabbix Agent (zabbix-agent.yml) - Tested on EL 6 and EL 7
* Zabbix Server (zabbix-server.yml) - Tested on EL 6 and EL 7

Included Utility Playbooks:
* Hostname (hostname.yml) - Tested on EL 6 and EL 7, used for setting up the hostname of the VM

## Pre-requirements
In order to use this set of playbooks, you need to install ansible on your ansible / admin server. Use your package manager and install ansible.

```
yum install ansible
```

Once this is done, next step is to configure passwordless authentication between the admin server and the machines you plan to control though ansible. Generate key and ssh-copy-id will do the trick. 

## Assumptions
* Names of the nodes are resolvable (between the nodes and on the ansible server). Use /etc/hosts files for this if needed.
* SELinux issues are not tackled by playbooks.
* IPtables configurations are not included in the playbooks as well.


## Using the playbooks
In order to use the playbooks, you need to follow few simple steps. 

1. Download the archive or checkout using git.
2. Enter the folder and check the variables defined into vars/node1.bluedrive.ro file. Define them in variable files at host level (in host_vars/host_name) or at group level (in group_vars/group_name)
3. Adjust the host inventory (myhosts file)
4. Run playbooks against your hosts to check differences

```
ansible-playbook -D -C -uroot -l node1.bluedrive.ro logstash.yml
```

5. Apply your playbooks to the hosts (remove -C flag)

```
ansible-playbook -D -uroot -l node1.bluedrive.ro logstash.yml
```


The playbooks need root access in order to complete successful. In case you have to connect directly as a different user and then perform privilege escalation, use the **-b** flag in order to force becoming root. Check the following example which works in Amazon EC2.

```
ansible-playbook -D -uec2-user -b -l cloud1.bluedrive.ro elasticsearch.yml
```

## Defining variables
In order to define the variables for your configurations, you have two options:
* define variables at group level, in group_vars/group_name . Yes, the new file should be the exact name of the group.
* define variables at host level, in host_vars/host_name . Once again, the file should be the exact name of the host in this case.

## More on playbooks
Let's describe a bit the existing playbooks and the variables needed for each of them.

### SugarCRM CE (sugarcrm.yml)
***Description***

Installs SugarCRM CE 6 on your server. Also it will configure a full LAMP machine, with HTTPD, MariaDB and PHP (standard version for your EL system).

***Variables***

Variable       |Example Value | Description
---------------|--------------|------------
sugarcrm_db|sugarcrm_db|Name of SugarCRM DB. Should not be created, the tool will deploy it.
sugarcrm_db_user|sugarcrm|Name of DB user for access to SugarCRM DB
sugarcrm_db_password|sugarcrm|Password for DB user
sugarcrm_db_host|127.0.0.1|Host where DB server is. If localhost, tool will configure the MariaDB locally
sugarcrm_install_folder|/var/www/html|Folder where application files should be installed
sugarcrm_admin_username|sugaradmin|SugarCRM Admin User for Web Interface
sugarcrm_admin_password|adminpass|SugarCRM Admin User's password.
mysql_root_user|root|mysql root user
mysql_root_password|root|mysql root password
httpd_certificate_file|keys/node1.crt|path to SSL certificate
httpd_key_file: keys/node1.key|path to SSL key
httpd_redirect_to_https|true|set to true in order to enable HTTP to HTTPS redirection. set it to false to disable SSL
full_hostname|node1.bluedrive.ro|hostname of the server
php_timezone|Europe/Bucharest|timezone of the server. Used for PHP configuration.

***Known Pitfalls / Errors***

Nothing so far.

### Nextcloud (nextcloud.yml)
***Description***

Installs Nextcloud 10 on your system. This playbook will deploy a dropbox-like system on your target server, ready to be used by users. Make sure enough space will be available under your nextcloud_data_folder. Also it will configure a full LAMP machine, with HTTPD, MariaDB and PHP 5.6 from Webtatic Repository.

***Variables***

Variable       |Example Value | Description
---------------|--------------|------------
nextcloud_db|nextcloud_db|name of DB to be used by Nextcloud
nextcloud_db_user|nextcloud|Mysql/MariaDB user for DB access
nextcloud_db_password|nextcloud|Password for nextcloud_db_user
nextcloud_db_host|127.0.0.1|DB host
nextcloud_install_folder|/var/www/html|folder where nextcloud php app will be unpacked
nextcloud_data_folder|/var/www/nextcloud|folder for storing the actual files uploaded by users
nextcloud_admin_password|admin|nextcloud admin username
nextcloud_admin_username|admin|nextcloud admin user password
mysql_root_user|root|mysql root user
mysql_root_password|root|mysql root password
httpd_certificate_file|keys/node1.crt|path to SSL certificate
httpd_key_file: keys/node1.key|path to SSL key
httpd_redirect_to_https|true|set to true in order to enable HTTP to HTTPS redirection. set it to false to disable SSL
full_hostname|node1.bluedrive.ro|hostname of the server
php_timezone|Europe/Bucharest|timezone of the server. Used for PHP configuration.

***Known Pitfalls / Errors***

Nothing so far.



### CachetHQ (cachethq.yml)
***Description***

Installs CachetHQ version 2.0.0 on your system. Also it will configure a full LAMP machine, with HTTPD, MariaDB and PHP 5.6 from Webtatic Repository

***Variables***

Variable       |Example Value | Description
---------------|--------------|------------
cachet_app_key |hnftgsdeiu87wruhxt65rgtsoprkjnas| some random 32 chars used internally by Laravel Framework 
cachet_db|cachetdb|name of database to use 
cachet_db_user|cachet|database username for DB access
cachet_db_password|cachet|database password for DB access
httpd_certificate_file|keys/node1.crt|path to local certificate file (on ansible host)
httpd_key_file|keys/node1.key|path to local key file (on ansible host)
httpd_redirect_to_https|true|if http to https redirection should be enabled
mysql_root_user|root|mariadb admin user (usually root)
mysql_root_password|root|mariabd admin/root password
php_timezone|Europe/Bucharest|set php timezone
smtp_gateway|smtp.bluedrive.ro|smtp server used for sending e-mails
smtp_port|25|smtp port 

***Known Pitfalls / Errors***

Requires SELinux adjustments, but now the playbook does everything for you.

### Elasticsearch (elasticsearch.yml)
***Description***

Deploys latest available Elasticsearch 2.x package on a new node. It supports automatic deployment of clusters and can be used for a large scale elasticsearch setup configuration.
 
***Variables***

Variable       |Example Value | Description
---------------|--------------|------------
elasticsearch_jvm_heap_size|512m|heap size of JVM running elasticsearch
elasticsearch_cluster_name|cluster01|name of cluster for the new node
elasticsearch_data_path|/var/lib/elasticsearch|path where data will be stored
elasticsearch_index_number_of_shards|2|number of active shards for indexes
elasticsearch_index_number_of_replicas|0|number of replicas for active shards
elasticsearch_cluster_members|'"node1.bluedrive.ro","node2.bluedrive.ro"'|list of cluster members, for unicast discovery
elasticsearch_minimum_masters|1|minimum number of nodes for picking up the masters. should be (number_of_cluster_members / 2) + 1. prevents split-brain
elasticsearch_multicast_enabled|true|set to true in order to enable multicast discovery or false to disable it

***Known Pitfalls / Errors***

Not yet.

### Logstash (logstash.yml)
***Description***

Installs latest Logstash release on the machine. Also configures Syslog receivers on ports 9514/tcp and 9514/udp. Adds one standard syslog filter and  one elasticsearch data backend in the configuration.

***Variables***

Variable       |Example Value | Description
---------------|--------------|------------
logstash_elasticsearch_node_list|'"node1.bluedrive.ro","node2.bluedrive.ro"'|list of backend elasticsearch nodes (nodes playing client roles)
logstash_jvm_heap_size|512m|heap size of JVM running logstash


***Known Pitfalls / Errors***

Not yet.

### Kibana (kibana.yml)
***Description***

Will install Kibana 4.3.0 and configure access to an elasticsearch backend. HAProxy is used as a proxy for protecting kibana aplication server.

***Variables***

Variable       |Example Value | Description
---------------|--------------|------------
kibana_elasticsearch_url|'http://node1.bluedrive.ro:9200'|url of existing elasticsearch node for query
kibana_user|alex|username used for http authentication
kibana_password|alex|password for kibana_user
haproxy_ssl_bundle|keys/haproxy.pem|local path to SSL key/certificate bundle used by HAProxy (local path on the ansible server)
haproxy_redirect_to_https|true|if redirection from http to https must be enabled


***Known Pitfalls / Errors***

Not yet.

### Jenkins (jenkins.yml)
***Description***

Installs Jenkins with HAProxy used as a proxy for protecting the Jenkins aplication server. SSL configuration is supported.

***Variables***

Variable       |Example Value | Description
---------------|--------------|------------
haproxy_local_proxy|true|configures the backend for an application server running on 127.0.0.1:8080
haproxy_ssl_bundle|keys/haproxy.pem|local path to SSL key/certificate bundle used by HAProxy (local path on the ansible server)
haproxy_redirect_to_https|true|if redirection from http to https must be enabled

***Known Pitfalls / Errors***

Not yet.

### Tomcat (tomcat.yml)
***Description***

Deploy Tomcat Web Application server ( 8.30 ). Support for SSL configuration is included, through the HAProxy instance deployed as reverse proxy.

***Variables***

Variable       |Example Value | Description
---------------|--------------|------------
tomcat_deploy_haproxy|true|if HAProxy should be deployed as a reverse proxy to protect the Tomcat
tomcat_jvm_heap_size|512m| Tomcat JVM heap size
haproxy_ssl_bundle|keys/haproxy.pem|local path to SSL key/certificate bundle used by HAProxy (local path on the ansible server)
haproxy_redirect_to_https|true|if redirection from http to https must be enabled

***Known Pitfalls / Errors***

Not yet.


### HTTPD (httpd.yml)
***Description***

Deploy Apache Web server ( Red Hat standard version ). Support for SSL configuration is included.

***Variables***

Variable       |Example Value | Description
---------------|--------------|------------
httpd_certificate_file|keys/node1.crt|path to local certificate file (on ansible host)
httpd_key_file|keys/node1.key|path to local key file (on ansible host)
httpd_redirect_to_https|true|if http to https redirection should be enabled


***Known Pitfalls / Errors***

Not yet.

### PHP STANDARD (php56.yml)
***Description***

Deploy Apache Web server and PHP ( Red Hat standard versions ). Support for SSL configuration is included.

***Variables***

Variable       |Example Value | Description
---------------|--------------|------------
httpd_certificate_file|keys/node1.crt|path to local certificate file (on ansible host)
httpd_key_file|keys/node1.key|path to local key file (on ansible host)
httpd_redirect_to_https|true|if http to https redirection should be enabled
php_timezone|Europe/Bucharest|set php timezone

***Known Pitfalls / Errors***

Not yet.

### PHP 56 (php56.yml)
***Description***

Deploy Apache Web server ( Red Hat standard version ) and PHP 5.6 compiled by Webtatic. Support for SSL configuration is included.

***Variables***

Variable       |Example Value | Description
---------------|--------------|------------
httpd_certificate_file|keys/node1.crt|path to local certificate file (on ansible host)
httpd_key_file|keys/node1.key|path to local key file (on ansible host)
httpd_redirect_to_https|true|if http to https redirection should be enabled
php_timezone|Europe/Bucharest|set php timezone

***Known Pitfalls / Errors***

Not yet.


### Hostname (hostname.yml)
***Description***

Used to set the hostname of an instance.

***Variables***

Variable       |Example Value | Description
---------------|--------------|------------
full_hostname|node1.bluedrive.ro|FQDN of the machine.

***Known Pitfalls / Errors***

Not yet.

### Zabbix Agent (zabbix-agent.yml)
***Description***

Deploy Zabbix Agent (2.4) and configure access from the Zabbix Server for monitoring. Can register hosts on existing Zabbix Servers. Note that the registering action is performed by the local ansible master by calling directly the Zabbix API of the server. In order to get this working, zabbix-api python module must be installed. You can use pip like **pip install zabbix-api** .

***Variables***

Variable       |Example Value | Description
---------------|--------------|------------
zabbix_server|127.0.0.1|IP address of the remote zabbix server
zabbix_agent_register_server|true|If the ansible master should attempt to register the host to an existing Zabbix server
zabbix_agent_nms_url|http://zabbix.bluedrive.ro/zabbix/|URL of the existing Zabbix server (required for API access)
zabbix_agent_nms_user|ansible|Username defined on Zabbix server for registering hosts (check permissions!)
zabbix_agent_nms_password|ansible|Password for the above mentioned user
zabbix_agent_nms_template|  - Template OS Linux|List of templates to be linked to the new host
zabbix_agent_nms_hostgroup|  - Linux servers|List of hostgroups where the new host will be added
zabbix_agent_interface_ip|10.15.0.21|IP of the Zabbix host, used for Zabbix Agent communication to server.

***Known Pitfalls / Errors***

Not yet.

### Zabbix Server (zabbix-server.yml)
***Description***

Deploy Zabbix Server (2.4) with Web Application. SSL Support is included through httpd vars.

***Variables***

Variable       |Example Value | Description
---------------|--------------|------------
httpd_certificate_file|keys/node1.crt|path to local certificate file (on ansible host)
httpd_key_file|keys/node1.key|path to local key file (on ansible host)
httpd_redirect_to_https|true|if http to https redirection should be enabled
php_timezone|Europe/Bucharest|set php timezone
zabbix_db|zabbixdb|zabbix database
zabbix_db_user|zabbix database user
zabbix_db_password|zabbix database user password

***Known Pitfalls / Errors***

Not yet.



## Reporting bugs
Write discovered bugs or improvement ideas at support@bluedrive.ro .
